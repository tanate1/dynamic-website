import express from 'express';
import {renderCart} from '../controllers/cartControllers.js';
const router = express.Router();

router.route("/").get(renderCart);


export default router;