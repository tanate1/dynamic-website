import express from 'express';
import {renderInfo,receiveUserInfo} from '../controllers/infoController.js';

const router = express.Router();

router.route("/").get(renderInfo).post(receiveUserInfo);


export default router;