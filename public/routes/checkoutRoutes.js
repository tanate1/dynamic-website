import express from 'express';
import {renderCheckout} from '../controllers/checkoutControllers.js'
const router = express.Router();


router.route("/").get(renderCheckout);


export default router;