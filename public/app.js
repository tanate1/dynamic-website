import express from 'express';
import homeRoute from './routes/homeRoute.js';

import bodyParser from 'body-parser';
import nodemailer from 'nodemailer';
import infoRoutes from './routes/infoRoutes.js';
import path from 'path';

const app = express();
const PORT = process.env.PORT || 3000;

app.set('view engine', 'ejs');

const __dirname = path.resolve();
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname,'public')));

app.use("/", homeRoute);


app.listen(PORT, ()=>console.log(`Server running in port ${PORT}`));