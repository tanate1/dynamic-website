

$(document).ready(function(){

    
 

$(".btn").click(function(e){
    var button = e.target;
    var item = button.parentElement.parentElement;
    var title = item.getElementsByClassName('card-title')[0].innerText;
    var price = item.getElementsByClassName('card-text')[0].innerText;
    var imageSrc = item.getElementsByClassName('card-img-top')[0].src;
    addToCart(title,price,imageSrc);
    updateCartTotal();
});

$("body").delegate(".btn-danger", "click", function(e){
    var buttonClicked = e.target
    buttonClicked.parentElement.parentElement.remove()
    updateCartTotal()
});
$("body").delegate(".cart-quantity-input", "change", function(e){
    var input = e.target;
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal();
});


const addToCart = (title, price, imageSrc)=>{
    var cartRow = document.createElement('div');
    var cartItems = document.getElementsByClassName("cart__products-wrapper")[0];
    var cartItemNames = cartItems.getElementsByClassName('product__name')
    for (var i = 0; i < cartItemNames.length; i++) {
        if (cartItemNames[i].innerText == title) {
            alert('This item is already added to the cart')
            return
        }
    }
    var cartContent =
    `
    <div class="cart__product">
        <img class="product__image" src="${imageSrc}">
        <div class="product__description">
            <h2 class="product__name">${title}</h2>
        </div>
        <div class="product__description">
            <h2 class="product__price">${price}</h2>
        </div>
        <div class="cart-quantity cart-column">
            <input class="cart-quantity-input" type="number" value="1">
            <button class="remove-btn btn btn-danger" type="button">REMOVE</button>
        </div>
    </div>
    `;
    cartRow.innerHTML = cartContent;
    cartItems.append(cartRow);
}

function updateCartTotal() {
    var cartItemContainer = document.getElementsByClassName('cart__products-wrapper')[0]
    var cartRows = cartItemContainer.getElementsByClassName('cart__product');
    var total = 0;
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var priceElement = cartRow.getElementsByClassName('product__price')[0];
        var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0];
        var price = parseFloat(priceElement.innerText.replace('$', ''));
        var quantity = quantityElement.value;
        total = total + (price * quantity)
    }

    document.getElementsByClassName('subtotal__totalPrice')[0].innerText = '$' + total
    document.getElementsByClassName('subtotal__itemCount')[0].innerText = "Subtotal" + "(" +cartRows.length+ ")";
    var subtotalForm = document.getElementsByClassName("subtotal-form");
    subtotalForm[0].value = cartRows.length;
    var subtotalTotalForm = document.getElementsByClassName("subtotal-totalForm");
    subtotalTotalForm[0].value = total;
}


});