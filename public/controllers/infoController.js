import nodemailer from 'nodemailer';


const renderInfo = (req,res) =>{
    res.render('userinfo');
}

const receiveUserInfo = (req,res)=>{
    const output = `
    <div style="text-align:center;padding:50px 20px">
      <h1 style="font-size:30px;font-weight:700;color:black">Hello ${req.body.name}</h1>
      <p style="color:#9A77F3;font-size:20px;">Your Purchase of $${req.body.total}(${req.body.itemcount} items) will be shipped to ${req.body.address},${req.body.city},${req.body.postal}</p>
    </div>
    `;

    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // upgrade later with STARTTLS
      auth: {
        user: "eduardotanate2@gmail.com",
        pass: "Ed123456789",
      },
      tls:{
        rejectUnauthorized:false
      }
    });

    let mailOptions = transporter.sendMail({
      from: '"Edssneaker Shop"<eduardotanate2@gmail.com>',
      to: `${req.body.email}`,
      subject: `Edssneaker Shop Purchase`,
      html: output
    });

    transporter.sendMail(mailOptions,()=>{
      res.render('home');
    });
};



export {renderInfo, receiveUserInfo};