import express from 'express';
import {renderHome} from '../controllers/homeControllers.js';
import {receiveUserInfo} from '../controllers/infoController.js';
const router = express.Router();

router.route("/").get(renderHome).post(receiveUserInfo);


export default router;